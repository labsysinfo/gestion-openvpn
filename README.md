# gestion-openvpn

Script to install and configure OpenVPN for Debian and Ubuntu.

## Install

With root rights :

	wget https://git.labsys.xyz/labsys/gestion-openvpn/archive/gestion-openvpn-1.3.0.tar.gz
	tar -zxvf gestion-openvpn-1.3.0.tar.gz
	mv gestion-openvpn/gestion-openvpn /usr/sbin/

## Usage

	gestion-openvpn install [OpenVPN server IP or FQDN]    Install OpenVPN.
	gestion-openvpn remote [OpenVPN server IP or FQDN]     Configures the ip address or FQDN address to connect to the OpenVPN server. /!\ After this command it is necessary to regenerate the OpenVPN profiles.
	gestion-openvpn port [number port] [tcp/udp]           Configures the port number of the OpenVPN server. /!\ After this command it is necessary to regenerate the OpenVPN profiles.
	gestion-openvpn network-ipv4 [ipv4] [subnet]           Configures the ipv4 local network of the OpenVPN server.
	gestion-openvpn network-ipv6 [ipv6/cidr]               Configures the ipv6 local network of the OpenVPN server.
	gestion-openvpn add-snat [interface]                   Add a source NAT rule on the interface.\n
	gestion-openvpn del-snat [interface]                   Delete a source NAT rule on the interface.\n
	gestion-openvpn add-user [user]                        Created an OpenVPN user.
	gestion-openvpn del-user [user]                        Delete an OpenVPN user.
	gestion-openvpn recreate-user-profil                   Recreated OpenVPN profiles.
	gestion-openvpn add-push-dns-ipv4 [ipv4]               Add an ipv4 DNS to the OpenVPN clients.
	gestion-openvpn add-push-dns-ipv6 [ipv6]               Add an ipv6 DNS to the OpenVPN clients.
	gestion-openvpn del-push-dns-ipv4 [ipv4]               Delete an ipv4 DNS to the OpenVPN clients.
	gestion-openvpn del-push-dns-ipv6 [ipv6]               Delete an ipv6 DNS to the OpenVPN clients.
	gestion-openvpn push-domain [domain]                   Configures an domain to the OpenVPN clients.
	gestion-openvpn add-push-route-ipv4 [ipv4] [subnet]    Add an ipv4 route to the OpenVPN clients.
	gestion-openvpn add-push-route-ipv6 [ipv6/cidr]        Add an ipv6 route to the OpenVPN clients.
	gestion-openvpn del-push-route-ipv4 [ipv4] [subnet]    Delete an ipv4 route to the OpenVPN clients.
	gestion-openvpn del-push-route-ipv6 [ipv6/cidr]        Delete an ipv6 route to the OpenVPN clients.
	gestion-openvpn show-config                            Displays the configuration OpenVPN sever.
	gestion-openvpn show-users                             Displays the list of OpenVPN users.
	gestion-openvpn show-dhcp-option                       Displays the list of OpenVPN server dhcp options.
	gestion-openvpn show-push-route                        Displays the list of route.
	gestion-openvpn help                                   Displays this message.

## Compatibility

The script is made to work on these OS and architectures :

- **Debian 9** (i386, amd64, armhf, arm64)
- **Ubuntu 18.04 LTS** (i386, amd64, armhf, arm64)

## Features

- Install OpenVPN
- Add and remove OpenVPN clients
- IPv4 and IPv6 compatible
- Configure the OpenVPN network
- Configure the route
- Configure the DNS
- Configure the domain